package br.com.itau.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Acesso consultado não encontrado.")
public class AcessoNotFoundException extends RuntimeException {
}
