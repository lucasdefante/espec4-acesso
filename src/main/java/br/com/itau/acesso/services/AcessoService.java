package br.com.itau.acesso.services;

import br.com.itau.acesso.exceptions.AcessoNotFoundException;
import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Optional;
import java.util.Random;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    private static final Duration timeoutTime = Duration.ofSeconds(10);

    private static final String[] TOPICOS = {"spec4-lucas-defante-1", "spec4-lucas-defante-2", "spec4-lucas-defante-3"};

    public Acesso criarAcesso(Acesso acesso) {
        try {
            return acessoRepository.save(acesso);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Acesso já cadastrado no sistema.");
        }
    }

    public Acesso consultarAcessoPorClienteIdEPortaId(int clienteId, int portaId) {
        Random rnd = new Random();
        Optional<Acesso> optionalAcesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);
        if (!optionalAcesso.isPresent()) {
            Acesso acesso = new Acesso();
            acesso.setPortaId(portaId);
            acesso.setClienteId(clienteId);
            enviarAoKafka(rnd.nextInt(TOPICOS.length), acesso);
            throw new AcessoNotFoundException();
        }
        Acesso acesso = optionalAcesso.get();
        enviarAoKafka(rnd.nextInt(TOPICOS.length), acesso);
        return acesso;
    }

    public void enviarAoKafka(int topicId, Acesso acesso) {
        producer.setCloseTimeout(timeoutTime);
        producer.send(TOPICOS[topicId], acesso);
    }

    public void deletarAcessoPorClienteIdEPortaId(int clienteId, int portaId) {
        Acesso acesso = consultarAcessoPorClienteIdEPortaId(clienteId, portaId);
        acessoRepository.delete(acesso);
    }
}
