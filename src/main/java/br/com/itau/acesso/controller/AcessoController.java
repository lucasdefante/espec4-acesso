package br.com.itau.acesso.controller;

import br.com.itau.acesso.clients.ClienteClient;
import br.com.itau.acesso.clients.PortaClient;
import br.com.itau.acesso.dtos.AcessoMapper;
import br.com.itau.acesso.dtos.ConsultarAcessoDTO;
import br.com.itau.acesso.dtos.CriarAcessoDTO;
import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.KafkaException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ConsultarAcessoDTO criarAcesso(@RequestBody @Valid CriarAcessoDTO criarAcessoDTO) {
        clienteClient.consultarClientePorId(criarAcessoDTO.getCliente_id());
        portaClient.consultarPortaPorId(criarAcessoDTO.getPorta_id());
        try {
            Acesso acesso = acessoMapper.toAcesso(criarAcessoDTO);
            Acesso acessoDB = acessoService.criarAcesso(acesso);
            return acessoMapper.toConsultarAcessoDTO(acesso);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{clienteId}/{portaId}")
    public ConsultarAcessoDTO consultarAcessoPorClienteIdEPortaId(@PathVariable(name = "clienteId") int clienteId,
                                                                  @PathVariable(name = "portaId") int portaId) {
        try {
            Acesso acesso = acessoService.consultarAcessoPorClienteIdEPortaId(clienteId, portaId);
            return acessoMapper.toConsultarAcessoDTO(acesso);
        } catch (KafkaException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcessoPorClienteIdEPortaId(@PathVariable(name = "clienteId") int clienteId,
                                                  @PathVariable(name = "portaId") int portaId) {
        acessoService.deletarAcessoPorClienteIdEPortaId(clienteId, portaId);
    }
}
