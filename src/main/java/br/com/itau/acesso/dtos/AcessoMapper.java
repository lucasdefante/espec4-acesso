package br.com.itau.acesso.dtos;

import br.com.itau.acesso.models.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso toAcesso(CriarAcessoDTO criarAcessoDTO) {
        Acesso acesso = new Acesso();
        acesso.setPortaId(criarAcessoDTO.getPorta_id());
        acesso.setClienteId(criarAcessoDTO.getCliente_id());
        return acesso;
    }

    public ConsultarAcessoDTO toConsultarAcessoDTO(Acesso acesso) {
        ConsultarAcessoDTO consultarAcessoDTO = new ConsultarAcessoDTO(acesso.getPortaId(), acesso.getClienteId());
        return consultarAcessoDTO;
    }
}
