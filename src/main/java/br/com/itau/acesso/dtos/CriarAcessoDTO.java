package br.com.itau.acesso.dtos;

import javax.validation.constraints.NotNull;

public class CriarAcessoDTO {

    @NotNull(message = "ID da Porta não deve ser nulo")
    private int porta_id;

    @NotNull(message = "ID do Cliente não deve ser nulo")
    private int cliente_id;

    public CriarAcessoDTO() {
    }

    public int getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(int porta_id) {
        this.porta_id = porta_id;
    }

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }
}
