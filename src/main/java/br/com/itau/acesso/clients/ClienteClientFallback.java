package br.com.itau.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public void consultarClientePorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE2 indisponível no momento. Tente novamente mais tarde.");
    }
}
