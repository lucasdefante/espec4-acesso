package br.com.itau.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PortaClientFallback implements PortaClient {

    @Override
    public void consultarPortaPorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço PORTA indisponível no momento. Tente novamente mais tarde.");
    }
}
