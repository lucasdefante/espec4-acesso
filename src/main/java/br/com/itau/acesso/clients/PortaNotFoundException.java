package br.com.itau.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta consultada não encontrada.")
public class PortaNotFoundException extends RuntimeException {
}
